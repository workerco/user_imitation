import requests
import json

with open('data/test_request_body.json', 'r') as file:
    body = json.load(file)

response = requests.post('http://0.0.0.0:80/api/suggestion',json=body)
print(response.json())
