from classifiers import GeneralMessageClassifier
from utils import preprocess_dialog_received
from pprint import pprint
from collections import defaultdict
import random

class UserSuggestionMatcher:

    def __init__(self,user_message2category):
        self.user_message2category = user_message2category

    def suggest(self, dialog):

        dialog.sort(key = lambda x: x['sent_at'])

        not_answered_categories = set()

        for item in dialog:
            if item['user'] == 'coach':
                if item['category']:
                    not_answered_categories.add(item['category'])
            if item['user'] == 'user':
                if item['category'] and item['category'] in not_answered_categories:
                    not_answered_categories.remove(item['category'])

        suggestions = []
        for category in not_answered_categories:
            suggestion = {}
            suggestion['text'] = random.choice(self.user_message2category[category])
            suggestions.append(suggestion)

        return suggestions



if __name__ == '__main__':
    import json

    with open('data/coach_messages2category.json', 'r') as file:
        coach_message2category = json.load(file)

    with open('data/user_message2category.json', 'r') as file:
        user_message2category = json.load(file)

    with open('data/test_request_body.json', 'r') as file:
        dialog = json.load(file)

    dialog = preprocess_dialog_received(dialog)

    classifier = GeneralMessageClassifier(user_message2category, coach_message2category)

    category2user_message  = defaultdict(list)
    for text in user_message2category.keys():
        category2user_message[user_message2category[text]['category']].append(text)

    dialog = classifier.categorize(dialog)

    suggester = UserSuggestionMatcher(category2user_message)

    pprint(suggester.suggest(dialog))
