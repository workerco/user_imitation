from pprint import pprint
from utils import preprocess_dialog_received

class GeneralMessageClassifier:

    def __init__(self, user_messages_marked, coach_messages_marked):
        self.user_messages_marked = user_messages_marked
        self.coach_messages_marked = coach_messages_marked

    def categorize(self, dialog):

        for item in dialog:


            if item['user'] == 'coach':

                if item['text'] in self.coach_messages_marked.keys():
                    item['category'] = self.coach_messages_marked[item['text']]['category']
                else:
                    item['category'] = None

            if item['user'] == 'user':

                if item['text'] in self.user_messages_marked.keys():
                    item['category'] = self.user_messages_marked[item['text']]['category']
                else:
                    item['category'] = None

        return dialog

if __name__ == '__main__':

    import json

    with open('data/coach_messages2category.json','r') as file:
        coach_message2category = json.load(file)

    with open('data/user_message2category.json', 'r') as file:
        user_message2category = json.load(file)

    with open('data/test_request_body.json', 'r') as file:
        dialog = json.load(file)

    dialog = preprocess_dialog_received(dialog)

    classifier = GeneralMessageClassifier(user_message2category,coach_message2category)

    pprint(classifier.categorize(dialog))



