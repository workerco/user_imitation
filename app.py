from ai_utils.agumention.human_imitation.generator import UserImitatorWithClassfication
from utils import preprocess_dialog_received
from classifiers import GeneralMessageClassifier
from suggester import UserSuggestionMatcher
from collections import defaultdict
import json
import os
#
from flask import Flask, request, jsonify
from flask_restful import Resource, Api, reqparse
from flask import send_from_directory
#


with open('data/coach_messages2category.json', 'r') as file:
    coach_message2category = json.load(file)

with open('data/user_message2category.json', 'r') as file:
    user_message2category = json.load(file)

# with open('data/test_request_body.json', 'r') as file:
#     dialog = json.load(file)
#
# dialog = preprocess_dialog_received(dialog)
#
#

category2user_message  = defaultdict(list)
for text in user_message2category.keys():
    category2user_message[user_message2category[text]['category']].append(text)

classifier = GeneralMessageClassifier(user_message2category, coach_message2category)
suggester = UserSuggestionMatcher(category2user_message)
human_imitation = UserImitatorWithClassfication(classifier,suggester)


app = Flask(__name__)

@app.route('/api/suggestion', methods=['POST'])
def suggestion():
    response = {}

    if 'data' not in request.json:
        response['status'] = "haven't data in body"
        return jsonify(response)


    suggested = human_imitation.suggest_new_phrases(preprocess_dialog_received(request.json))

    response['suggestion'] = suggested
    response['status'] = 'ok'

    return jsonify(response)
if __name__ == '__main__':
    with open('configs/app.config','r') as file:
        config = json.load(file)

    adder = os.environ.get('NODE_APP_INSTANCE')
    adder = int(adder) if adder else 0
    starting_port = adder + config['port']
    app.run(host=config['host'], port=starting_port)
