from pprint import pprint

def preprocess_dialog_received(item):
    dialog = item['data']
    processed_dialog = []

    selected_from_body = ['sent_at','type','text']

    for item in dialog:
        new_item = {}
        new_item['sent_at'] = item['body']['sent_at']
        new_item['user'] = 'user' if item['body']['type'] == 1 else 'coach'
        new_item['text'] = item['body']['text']
        processed_dialog.append(new_item)

    return processed_dialog


if __name__ == '__main__':
    import json

    with open('data/test_request_body.json', 'r') as file:
        dialog = json.load(file)

    pprint(preprocess_dialog_received(dialog))